---
title: About
permalink: about/
profile: true
---

[Photographer](http://arenzanaphotography.com) and [Systems guy](http://trnswrks.com).
Interested in everything: from art to technology, faith to travel.

Spanish at heart, American by residence.

Contact me at [iarenzana@gmail.com](mailto://iarenzana@gmail.com).

PGP Fingerprint {{< highlight bash >}} AFB2 336F 7EA5 BE5A B9B2  2FF8 2B20 1107 DA00 4171 {{< /highlight >}} and [PGP Public Key](https://raw.githubusercontent.com/iarenzana/iarenzana.github.io/master/assets/misc/rsa_public_key.txt).
